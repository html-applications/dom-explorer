var $DOMExplorer = {

	version: '2.2.0',
	oTarget: {},
	oDialog: {},
	oElementExplore: {},
	oElementExploreName: '',
	oNavigationTable: {},
	oPropsTable: {},
	oHTMLHelp: new ActiveXObject("Internet.HHCtrl.1"),
	saveHeight: 0,
	heightCorrector: 0,

	// Удаление всех детей элемента
	fRemoveChilds: function (node){
		while (node.firstChild) node.removeChild(node.firstChild);
	},

	// Создание элемента с тегом tag и свойствами {props}
	// Свойство style фактически задает значение style.cssText
	fCreateElement: function (tag, props){
		var oElem = this.oDialog.document.createElement((tag.toUpperCase()=='CHECKBUTTON') ? 'DIV': tag);
		for (var nm in props){
			if (nm == 'style'){
				oElem.style.cssText = props.style;
			}else{
				oElem[nm] = props[nm];
			}
		}
		if (tag.toUpperCase()=='CHECKBUTTON') {
			oElem.setOnClick = oElem.onclick;
			oElem.checked = false;
			oElem.style.border = '2px outset';
			oElem.onclick = function () {
				if (oElem.checked){
					oElem.checked = false;
					oElem.style.border = '2px outset';
				} else {
					oElem.checked = true;
					oElem.style.border = '2px inset';
				}
				if (this.setOnClick) this.setOnClick();
			};
		}
		return oElem;
	},

	// Увеличение ширины диалогового окна при заполнении навигационной панели кнопками
	fDialogResize: function(oDOMExplorer) {
		return function(){
			var dialogWidth = Number(oDOMExplorer.oDialog.dialogWidth.replace(/\D*/g,''));
			if (oDOMExplorer.oNavigationTable.offsetWidth < dialogWidth) return;
			oDOMExplorer.oDialog.dialogWidth = oDOMExplorer.oDialog.document.body.scrollWidth + dialogWidth - oDOMExplorer.oDialog.document.body.offsetWidth + "px";
		};
	},

	// Установка фиксированной высоты ячейки со списком свойств объекта (срабатывает при изменении высоты диалогового окна)
	fCellPropsResize: function(oDOMExplorer){
		return function(){
			var dialogHeight = Number(oDOMExplorer.oDialog.dialogHeight.replace(/\D*/g,''));
			if (dialogHeight != oDOMExplorer.saveHeight) {
				oDOMExplorer.oDialog.idPropsCell.style.height = dialogHeight - 65 - oDOMExplorer.heightCorrector - ((oDOMExplorer.oDialog.idImm.style.display === '') ? 200 : 0);
				oDOMExplorer.saveHeight = dialogHeight;
			}
		};
	},

	// Подсветка выбранного элемента в целевом документе
	keepColor: '', keepBgColor: '',
	fElementHighlight: function(oDOMExplorer, elem, cnt, tmp){
		if (typeof(cnt)=='undefined'){
			cnt = 11;
			try {
				oDOMExplorer.keepColor = elem.style.color;
				oDOMExplorer.keepBgColor = elem.style.backgroundColor;
			} catch(e) {return;}
		}
		if (cnt>0) {
			elem.style.color =           (cnt & 1) ? 'red' : 'yellow';
			elem.style.backgroundColor = (cnt & 1) ? 'yellow' : 'red';
			cnt--;
			setTimeout(function(){oDOMExplorer.fElementHighlight(oDOMExplorer,elem,cnt,tmp);}, 100);
		} else {
			elem.style.color = oDOMExplorer.keepColor;
			elem.style.backgroundColor = oDOMExplorer.keepBgColor;
		}
	},

	// Создание таблицы стилей
	fAddStyleSheet: function (){
		var oSS = this.oDialog.document.createStyleSheet();
		oSS.addRule('*','font:8pt MS Shell Dlg;');
		oSS.addRule('html','height:100%;');
		oSS.addRule('body','height:100%; margin:0;');
		oSS.addRule('table','border-width:0; border-collapse:collapse; background-color:buttonface; font:menu;');
		oSS.addRule('td','white-space:nowrap; padding:1px 2px;');

		oSS.addRule('#idMainTable','height:100%; width:100%');
		oSS.addRule('.nav','height:28px; border-bottom:1px outset;');

		oSS.addRule('#idPropsCell','width:100%; vertical-align:top;');
		oSS.addRule('.props','height:100%; overflow-x:hidden; overflow-y:scroll; width:100%;');
		oSS.addRule('.props tr:hover','background-color:buttonhighlight;');
		oSS.addRule('.propsBtn','width:150px;');

		oSS.addRule('.toolCell','height:28px; border-top:1px outset;');
		oSS.addRule('.toolCell td','text-align:center;');
		oSS.addRule('#idGrab','width:20px; height:18px; font:normal 18px Wingdings; text-align:center;');
		oSS.addRule('#idGrab:hover','background-color:buttonhighlight;');
		oSS.addRule('.chkBtn','width:20px; height:18px; font:bold 12px monospace; text-align:center;');
		oSS.addRule('.chkBtn:hover','background-color:buttonhighlight;');
		oSS.addRule('.tBtn','width:22px; height:22px; font:normal 12px verdana; text-align:center;');
		oSS.addRule('.wBtn','width:22px; height:22px; font:normal 14px Wingdings; text-align:center;');
		oSS.addRule('#idImm','height:200px;');
		oSS.addRule('textarea','width:99%; height:195px; font:12px monospace;');
		oSS.addRule('textarea','width:99%; font:12px monospace;');
		oSS.addRule('.separator','width:0; border:1px inset; padding:0;');
	},

// = = = = = N A V I G A T I O N = = = = =

	// Обработка нажатия на кнопку в панели навигации
	fNavigationButtonOnClick: function (oDOMExplorer){
		return function () {
			while (this.parentNode.nextSibling) this.parentNode.parentNode.deleteCell(); // remove cells to right
			oDOMExplorer.oElementExplore = this.srcObject;
			oDOMExplorer.fShowElementProps();
			oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
		};
	},

	// Добавление кнопки с именем объекта в панель навигации
	fAddNavigationButton: function (){
		var oRow = this.oNavigationTable.rows[0] || this.oNavigationTable.insertRow();
		var oCell = oRow.insertCell();
		var oInput = this.fCreateElement("INPUT", {
			type: 'button',
			value: this.oElementExploreName,
			srcObject: this.oElementExplore,
			onclick: this.fNavigationButtonOnClick(this)
		});
		oCell.appendChild(oInput);
		setTimeout(this.fDialogResize(this), 0);
	},

// = = = = = P R O P E R T I E S = = = = =

	// Одинарный клик на ячейке с именем свойства (в списке свойств) => Подсветка элемента в целевом докумнте
	fPropNameOnClick: function(oDOMExplorer){
		return function () {
			oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore[this.innerText]);
		};
	},

	// Двойной клик на ячейке с именем свойства (в списке свойств) => Копирование полного имени свойства и его значения в Clipboard
	fPropNameOnDblClick: function(oDOMExplorer){
		return function () {
			var pname = function(){
				var tmp = []; var arr = oDOMExplorer.oNavigationTable.rows[0].cells;
				for(var i = 0, cell; cell = arr[i++];) tmp.push(cell.firstChild.value);
				return tmp.join('.');
			}(); pname = pname.replace(/\.(\d+)/g, '[$1]') + '.' + this.innerText;
			var pval = (this.nextSibling.childNodes[0].type == 'button') ? '{}' : '\'' + this.nextSibling.childNodes[0].value + '\'';
			clipboardData.setData("Text", pname + ' = ' + pval);
			oDOMExplorer.oHTMLHelp.TextPopup(pname + ' = ' + pval + '\nCopy to Clipboard!', "MS Sans Serif, 8, , plain", 4, 4, -1, -1);
		};
	},

	// Клик на кнопке с объектом => Смена текущего отображаемого элемента
	fObjectButtonOnClick: function(oDOMExplorer){
		return function () {
			var pname = this.parentNode.parentNode.cells[0].innerText;
			oDOMExplorer.oElementExplore = oDOMExplorer.oElementExplore[pname];
			oDOMExplorer.oElementExploreName = pname;
			oDOMExplorer.fAddNavigationButton();
			oDOMExplorer.fShowElementProps();
			oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
		};
	},

	// изменение значения свойства в целевом документе (при двойном клике на поле со значением)
	fPropValueOnDblClick: function(oDOMExplorer){
		return function(){
			try {
				oDOMExplorer.oElementExplore[this.parentNode.previousSibling.innerText] = this.value;
			} catch(e) {
				oDOMExplorer.oDialog.alert(e.message);
				this.value = oDOMExplorer.oElementExplore[this.parentNode.previousSibling.innerText];
			}
		};
	},

	// изменение значения свойства в целевом документе (при нажатии на Enter на поле со значением)
	fPropValueOnKeyPress: function(oDOMExplorer){
		return function (){
			if ((oDOMExplorer.oDialog.event.keyCode == 10)||(oDOMExplorer.oDialog.event.keyCode == 13)) {
				try {
					oDOMExplorer.oElementExplore[this.parentNode.previousSibling.innerText] = this.value;
				} catch(e) {
					oDOMExplorer.oDialog.alert(e.message);
					this.value = oDOMExplorer.oElementExplore[this.parentNode.previousSibling.innerText];
				}
				oDOMExplorer.oDialog.event.returnValue = false;
			}
		};
	},

	// Создание списка свойств текущего элемента
	fShowElementProps: function () {
		this.oDialog.idFirstChild.disabled =           !('firstChild' in this.oElementExplore);
		this.oDialog.idPreviousSibling.disabled =      !('previousSibling' in this.oElementExplore);
		this.oDialog.idParentNode.disabled =           !('parentNode' in this.oElementExplore);
		this.oDialog.idNextSibling.disabled =          !('nextSibling' in this.oElementExplore);
		this.oDialog.idLastChild.disabled =            !('lastChild' in this.oElementExplore);
		this.oDialog.idChildNodes.disabled =           !('childNodes' in this.oElementExplore);
		this.oDialog.idGetElementsByTagName.disabled = !('getElementsByTagName' in this.oElementExplore);

		var aTmp=[], val;
		for (var prop in this.oElementExplore) {
			try {val = this.oElementExplore[prop];} catch(e) {}
			if (val || this.oDialog.idShowNullChk.checked) aTmp.push([prop, val]);
		}
		aTmp.sort(function (a, b) {
				if (a[0]=='nodeName') return -1;
				if (b[0]=='nodeName') return 1;
				if ((Number(a[0])==a[0])&&(Number(b[0])==b[0])) return a[0]-b[0];
				return a[0].toLowerCase().localeCompare(b[0].toLowerCase());
		});
		this.fRemoveChilds(this.oPropsTable);
		var oRow, oCell;
		for(var i = 0, nameAndVal; nameAndVal = aTmp[i++];) {
			oRow = this.oPropsTable.insertRow();
			oCell = oRow.insertCell();
			oCell.style.width = '20%';
			oCell.innerHTML = nameAndVal[0];
			oCell.onclick = this.fPropNameOnClick(this);
			oCell.ondblclick = this.fPropNameOnDblClick(this);
			oCell = oRow.insertCell();
			var oInput = this.oDialog.document.createElement("INPUT");
			if (typeof(nameAndVal[1]) == 'object') {
				oInput.type = 'button';
				oInput.className = 'propsBtn';
				oInput.value = function(elem) {
					if (!elem) return 'object (null)';
					var tag = elem.nodeName;
					tag = tag ? ('<'+tag+'>') : 'object';
					var count = function(o){
						var c = 0;
						for (var p in o) try {if (o[p]) c++;} catch(e) {}
						return c;
					}(elem);
					return tag + ' ('+count+')';
				}(nameAndVal[1]);
				oInput.onclick = this.fObjectButtonOnClick(this);
			} else {
				oInput.type = 'text';
				oInput.value = nameAndVal[1];
				if (nameAndVal[1] && (nameAndVal[1].length > 60)) oInput.title = nameAndVal[1];
				oInput.style.width = '100%';
				oInput.ondblclick = this.fPropValueOnDblClick(this);
				oInput.onkeypress = this.fPropValueOnKeyPress(this);
			}
			oCell.appendChild(oInput);
		}
	},

// = = = = = T O O L = = = = =

	// Кнопка "Grab": Ручной выбор элемента на целевом документе (выбирается мышью при нажатом Ctrl)
	keepTargetEvent: {},
	fExploreTarget: function(oDOMExplorer){
		return function() {
			if (this.checked) {
				oDOMExplorer.keepTargetEvent = oDOMExplorer.oTarget.document.onmouseover || null;
				oDOMExplorer.oTarget.document.onmouseover = function() {
					if (event.ctrlKey) {
						oDOMExplorer.oElementExplore = event.srcElement;
						event.srcElement.style.cursor = 'crosshair';
						oDOMExplorer.oElementExploreName = 'srcElement';
						oDOMExplorer.fRemoveChilds(oDOMExplorer.oNavigationTable);
						oDOMExplorer.fAddNavigationButton();
						oDOMExplorer.fShowElementProps();
					} else {
						event.srcElement.style.cursor = '';
					}
					if (oDOMExplorer.keepTargetEvent) oDOMExplorer.keepTargetEvent();
				};
			} else { //off
				oDOMExplorer.oTarget.document.onmouseover = oDOMExplorer.keepTargetEvent ? oDOMExplorer.keepTargetEvent : null;
			}
		};
	},

	// Кнопка "h": Включение/отключение показа свойств без установленного значения (точнее - просто перезапуск fShowElementProps, которая проверяет нажата или нет эта кнопка)
	fShowEmptyValues: function(oDOMExplorer) {
		return function(){
			oDOMExplorer.fShowElementProps();
		};
	},

	// Нажатие на кнопку "firstChild"
	fFirstChild: function (oDOMExplorer){
		return function () {
			oDOMExplorer.oElementExplore = oDOMExplorer.oElementExplore.firstChild;
			oDOMExplorer.oElementExploreName = 'firstChild';
			oDOMExplorer.fAddNavigationButton();
			oDOMExplorer.fShowElementProps();
			oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
		};
	},

	// Нажатие на кнопку "previousSibling"
	fPreviousSibling: function (oDOMExplorer){
		return function () {
			oDOMExplorer.oElementExplore = oDOMExplorer.oElementExplore.previousSibling;
			oDOMExplorer.oElementExploreName = 'previousSibling';
			oDOMExplorer.fAddNavigationButton();
			oDOMExplorer.fShowElementProps();
			oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
		};
	},

	// Нажатие на кнопку "parentNode"
	fParentNode: function (oDOMExplorer){
		return function () {
			oDOMExplorer.oElementExplore = oDOMExplorer.oElementExplore.parentNode;
			oDOMExplorer.oElementExploreName = 'parentNode';
			oDOMExplorer.fAddNavigationButton();
			oDOMExplorer.fShowElementProps();
			oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
		};
	},

	// Нажатие на кнопку "getElementById"
	fGetElementById: function (oDOMExplorer){
		return function () {
			var id = prompt('document.getElementById("id")\nEnter Element ID:', '');
			if (id) {
				var elById = document.getElementById(id);
				if (elById) {
					oDOMExplorer.fRemoveChilds(oDOMExplorer.oNavigationTable);

					oDOMExplorer.oElementExplore = window;
					oDOMExplorer.oElementExploreName = 'window';
					oDOMExplorer.fAddNavigationButton();
					oDOMExplorer.oElementExplore = document;
					oDOMExplorer.oElementExploreName = 'document';
					oDOMExplorer.fAddNavigationButton();

					oDOMExplorer.oElementExplore = elById;
					oDOMExplorer.oElementExploreName = 'getElementById("'+id+'")';
					oDOMExplorer.fAddNavigationButton();
					oDOMExplorer.fShowElementProps();
					oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
				} else {
					oDOMExplorer.oDialog.alert('Document does not contain element with id = "'+id+'"');
				}
			}
		};
	},

	// Нажатие на кнопку "nextSibling"
	fNextSibling: function(oDOMExplorer){
		return function () {
			oDOMExplorer.oElementExplore = oDOMExplorer.oElementExplore.nextSibling;
			oDOMExplorer.oElementExploreName = 'nextSibling';
			oDOMExplorer.fAddNavigationButton();
			oDOMExplorer.fShowElementProps();
			oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
		};
	},

	// Нажатие на кнопку "lastChild"
	fLastChild: function (oDOMExplorer){
		return function () {
			oDOMExplorer.oElementExplore = oDOMExplorer.oElementExplore.lastChild;
			oDOMExplorer.oElementExploreName = 'lastChild';
			oDOMExplorer.fAddNavigationButton();
			oDOMExplorer.fShowElementProps();
			oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
		};
	},

	// Нажатие на кнопку "childNodes"
	fChildNodes: function (oDOMExplorer){
		return function () {
			oDOMExplorer.oElementExplore = oDOMExplorer.oElementExplore.childNodes;
			oDOMExplorer.oElementExploreName = 'childNodes';
			oDOMExplorer.fAddNavigationButton();
			oDOMExplorer.fShowElementProps();
			oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
		};
	},

	// Нажатие на кнопку "getElementsByTagName"
	fGetElementsByTagName: function (oDOMExplorer){
		return function () {
			var tag = prompt('element.getElementsByTagName("tag")\nEnter Elements Tag:', '*');
			if (tag) {
				try {
					var elementsByTag = oDOMExplorer.oElementExplore.getElementsByTagName(tag);
				} catch(e) {
					oDOMExplorer.oDialog.alert(e.message);
					return;
				}
				if (elementsByTag.length > 0) {
					oDOMExplorer.oElementExplore = elementsByTag;
					oDOMExplorer.oElementExploreName = 'getElementsByTagName("'+tag+'")';
					oDOMExplorer.fAddNavigationButton();
					oDOMExplorer.fShowElementProps();
					oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
				} else {
					oDOMExplorer.oDialog.alert('Current Element does not contain elements with tag = "'+tag+'"');
				}
			}
		};
	},

	// Нажатие на кнопку "getElementsByName"
	fGetElementsByName: function (oDOMExplorer){
		return function () {
			var name = prompt('document.getElementsByName("tag")\nEnter Elements Name (or ID):', '');
			if (name) {
				try {
					var elementsByName = document.getElementsByName(name);
				} catch(e) {
					oDOMExplorer.oDialog.alert(e.message);
					return;
				}
				if (elementsByName.length > 0) {
					oDOMExplorer.fRemoveChilds(oDOMExplorer.oNavigationTable);

					oDOMExplorer.oElementExplore = window;
					oDOMExplorer.oElementExploreName = 'window';
					oDOMExplorer.fAddNavigationButton();
					oDOMExplorer.oElementExplore = document;
					oDOMExplorer.oElementExploreName = 'document';
					oDOMExplorer.fAddNavigationButton();

					oDOMExplorer.oElementExplore = elementsByName;
					oDOMExplorer.oElementExploreName = 'getElementsByName("'+name+'")';
					oDOMExplorer.fAddNavigationButton();
					oDOMExplorer.fShowElementProps();
					oDOMExplorer.fElementHighlight(oDOMExplorer, oDOMExplorer.oElementExplore);
				} else {
					oDOMExplorer.oDialog.alert('Document does not contain elements with name (or id) = "'+name+'"');
				}
			}
		};
	},

// = = = = = I M M E D I A T E = = = = =

	// Кнопка "Immediate": Включение/отключение показа панели Immediate
	fShowImmediate: function (oDOMExplorer){
		return function(){
			oDOMExplorer.oDialog.idImm.style.display = this.checked ? '' : 'none';
			oDOMExplorer.oDialog.dialogHeight = (oDOMExplorer.oDialog.document.body.offsetHeight + (this.checked ? 200 : -200)) + 'px';
		};
	},

	// Обработка нажатия клавиш в панели Immediate
	fImmOnKey: function (oDOMExplorer){
		return function (){
			if (oDOMExplorer.oDialog.event.keyCode == 10) { // Ctrl+Enter
				oDOMExplorer.fExecScript(oDOMExplorer, this.innerText);
				oDOMExplorer.oDialog.event.returnValue = false;
			}
		};
	},

	// Обработка двойного клика мыши в панели Immediate
	fImmOnDblClick: function (oDOMExplorer){
		return function () {oDOMExplorer.fExecScript(oDOMExplorer, this.innerText);};
	},

	// Запуск пользовательского кода из панели Immediate
	fExecScript: function(oDOMExplorer, script){
		try {
			oDOMExplorer.oDialog.alert('Return value: ' + eval(script));
		} catch(err) {
			oDOMExplorer.oDialog.alert('Error: ' + err.message);
		}
	},

// = = = = = = = = = = = = = = = = = = =

	// Наполнение диалогового окна элементами (срабатывает 1 раз при старте)
	fCreateDialog: function (){
		var oTable = this.fCreateElement("TABLE", {id:'idMainTable'});
			var rowNav = oTable.insertRow();
				var cell0rNav = rowNav.insertCell();
				cell0rNav.className = 'nav';
					this.oNavigationTable = this.oDialog.document.createElement("TABLE");
					cell0rNav.appendChild(this.oNavigationTable);

			var rowProps = oTable.insertRow();
				var cell0rProps = rowProps.insertCell();
					cell0rProps.id = 'idPropsCell';
					var oDiv = this.fCreateElement("DIV", {className:'props'});
						this.oPropsTable = this.fCreateElement("TABLE", {style:'width:100%'});
						oDiv.appendChild(this.oPropsTable);
					cell0rProps.appendChild(oDiv);

			var rowTool = oTable.insertRow();
				var cell0rTool = rowTool.insertCell();
				cell0rTool.className = 'toolCell';
					var oTool = this.fCreateElement("TABLE", {style:'width:100%'});
						var row2Tool = oTool.insertRow();
							var cellr2Tool = row2Tool.insertCell();
								var oButton = this.fCreateElement("CHECKBUTTON", {
									innerHTML: '&#xB0;',
									title: 'Press Ctrl and mouse move on the target element!',
									id: 'idGrab',
									onclick: this.fExploreTarget(this)
								});
								cellr2Tool.appendChild(oButton);

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("CHECKBUTTON", {
									className: 'chkBtn',
									innerHTML: 'h',
									title: 'Show empty values',
									id: 'idShowNullChk',
									onclick: this.fShowEmptyValues(this)
								});
								cellr2Tool.appendChild(oButton);

								// - - - - -   s e p a r a t o r   - - - - -
							cellr2Tool = row2Tool.insertCell();
							cellr2Tool.className = 'separator';

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("INPUT", {
									type: 'button',
									id: 'idFirstChild',
									value: String.fromCharCode(0xE5),
									title: 'firstChild',
									className: 'wBtn',
									onclick: this.fFirstChild(this)
								});
								cellr2Tool.appendChild(oButton);

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("INPUT", {
									type: 'button',
									id: 'idPreviousSibling',
									value: String.fromCharCode(0xDF),
									title: 'previousSibling',
									className: 'wBtn',
									onclick: this.fPreviousSibling(this)
								});
								cellr2Tool.appendChild(oButton);

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("INPUT", {
									type: 'button',
									id: 'idParentNode',
									value: String.fromCharCode(0xE1),
									title: 'parentNode',
									className: 'wBtn',
									onclick: this.fParentNode(this)
								});
								cellr2Tool.appendChild(oButton);

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("INPUT", {
									type: 'button',
									value: '?',
									title: 'document.getElementById',
									className: 'tBtn',
									onclick: this.fGetElementById(this)
								});
								cellr2Tool.appendChild(oButton);

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("INPUT", {
									type: 'button',
									id: 'idNextSibling',
									value: String.fromCharCode(0xE0),
									title: 'nextSibling',
									className: 'wBtn',
									onclick: this.fNextSibling (this)
								});
								cellr2Tool.appendChild(oButton);

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("INPUT", {
									type: 'button',
									id: 'idLastChild',
									value: String.fromCharCode(0xE6),
									title: 'lastChild',
									className: 'wBtn',
									onclick: this.fLastChild (this)
								});
								cellr2Tool.appendChild(oButton);

								// - - - - -   s e p a r a t o r   - - - - -
							cellr2Tool = row2Tool.insertCell();
							cellr2Tool.className = 'separator';

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("INPUT", {
									type: 'button',
									id: 'idChildNodes',
									value: '[c]',
									title: 'childNodes',
									className: 'tBtn',
									onclick: this.fChildNodes (this)
								});
								cellr2Tool.appendChild(oButton);

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("INPUT", {
									type: 'button',
									id: 'idGetElementsByTagName',
									value: '[t]',
									title: 'getElementsByTagName',
									className: 'tBtn',
									onclick: this.fGetElementsByTagName (this)
								});
								cellr2Tool.appendChild(oButton);

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("INPUT", {
									type: 'button',
									value: '[n]',
									title: 'document.getElementsByName',
									className: 'tBtn',
									onclick: this.fGetElementsByName(this)
								});
								cellr2Tool.appendChild(oButton);

								// - - - - -   s e p a r a t o r   - - - - -
							cellr2Tool = row2Tool.insertCell();
							cellr2Tool.className = 'separator';

							cellr2Tool = row2Tool.insertCell();
								oButton = this.fCreateElement("CHECKBUTTON", {
									className: 'chkBtn',
									innerHTML: 'i',
									title: 'Immediate Window',
									onclick: this.fShowImmediate(this)
								});
								cellr2Tool.appendChild(oButton);

					cell0rTool.appendChild(oTool);

			var rowImm = oTable.insertRow();
				var cell0rImm = rowImm.insertCell();
				cell0rImm.id = 'idImm';
				cell0rImm.style.display = 'none';
					var oTextArea = this.fCreateElement("TEXTAREA", {
						title: 'Press Ctrl+Enter (or DoubleClick) to run this script',
						onkeypress: this.fImmOnKey(this),
						ondblclick: this.fImmOnDblClick(this)
					});
					cell0rImm.appendChild(oTextArea);

		this.oDialog.document.body.appendChild(oTable);

	},

	// Запуск данного скрипта
	fExplore: function () {
		this.oTarget = window;
		this.oDialog = this.oTarget.showModelessDialog("about:blank", "XXX", "resizable:yes; scroll:no; status:no; help:no; center:yes; dialogTop:50px; dialogLeft:150px; dialogWidth:400px; dialogHeight:500px;");
		this.oDialog.document.write('<!DOCTYPE html><html><head><meta http-equiv=MSThemeCompatible content=yes><body></body></html>');
		this.fAddStyleSheet();
		this.oDialog.document.title = 'DOM Explorer v.' + this.version;
		this.fCreateDialog();
		this.oElementExplore = this.oTarget;
		this.oElementExploreName = 'window';
		this.fAddNavigationButton();
		this.fShowElementProps();
		var hc = Number(this.oDialog.dialogHeight.replace(/\D*/g,'')) - this.oDialog.document.body.offsetHeight;
		this.heightCorrector = hc ? (hc - 3) : 0;
		this.oDialog.setInterval(this.fCellPropsResize(this), 500);
	}
}