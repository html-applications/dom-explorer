;@Ahk2Exe-SetName DOM Explorer
;@Ahk2Exe-SetDescription Explore DOM of HTML documents
;@Ahk2Exe-SetVersion 2.3.0.0
;@Ahk2Exe-SetCopyright mozers™
;@Ahk2Exe-SetOrigFilename DOMExp.exe
;@Ahk2Exe-SetCompanyName mozers™
;@Ahk2Exe-SetMainIcon DOMExp.ico
;@Ahk2Exe-AddResource DOMExp.js

#Warn
#NoEnv
#NoTrayIcon
#SingleInstance Force
SetBatchLines -1

arr := []
WinGet, id, list,,, Program Manager
Loop, %id%
{
	this_id := id%A_Index%
	WinGetClass, this_class, ahk_id %this_id%
	If (this_class = "IEFrame") {
		WinGetTitle, this_title, ahk_id %this_id%
		arr.push(["IE",this_title])
	}
	Else If (this_class = "HTML Application Host Window Class") {
		WinGetTitle, this_title, ahk_id %this_id%
		arr.push(["HTA",this_title])
	}
}

acount := arr.MaxIndex()
If (!acount) {
	MsgBox 0x30, DOM Explorer, Not HTA or Internet Explorer window found!
	ExitApp
}
Else If (acount = 1){
	COM_Connect(arr[1][2])
	ExitApp
}
Else {
	Loop % acount {
		Menu, MyMenu, Add, % arr[A_Index][1] "`t" arr[A_Index][2], MenuHandler
	}
	Menu, MyMenu, Show
	return
}

MenuHandler:
	COM_Connect(RegExReplace(A_ThisMenuItem, "^\S*?\t", ""))
ExitApp

COM_Connect(win_title){
	pwin := WBGet(win_title)
	If (pwin){
		If (A_IsCompiled) {
			TxtRes := FI_GetResource("DOMExp.js")
			jstext := StrGet(TxtRes.Ptr, TxtRes.Size, "UTF-8")
		} else {
			FileRead, jstext, % A_ScriptDir "\DOMExp.js"
			vSize := StrPut(jstext, "CP0")
			VarSetCapacity(vUtf8, vSize)
			vSize := StrPut(jstext, &vUtf8, vSize, "CP0")
			jstext := StrGet(&vUtf8, "UTF-8")
		}
		jscript := pwin.document.createElement("script")
		jscript.text := jstext
		head := pwin.document.getElementsByTagName("head")[0]
		head.appendChild(jscript, head)
		pwin.document.parentWindow.execScript("$DOMExplorer.fExplore()")
		WinActivate, %win_title%
	} else {
		MsgBox 0x30, DOM Explorer, Could not connect to DOM!
	}
}

; https://autohotkey.com/board/topic/47052-basic-webpage-controls-with-javascript-com-tutorial/
WBGet(WinTitle="ahk_class IEFrame", Svr#=1) {               ; based on ComObjQuery docs
	static msg := DllCall("RegisterWindowMessage", "str", "WM_HTML_GETOBJECT")
		, IID := "{332C4427-26CB-11D0-B483-00C04FD90119}"   ; IID_IHTMLWindow2 (работает с IE и с HTA)
; 		, IID := "{0002DF05-0000-0000-C000-000000000046}"   ; IID_IWebBrowserApp  (работает только с IE)
	SendMessage msg, 0, 0, Internet Explorer_Server%Svr#%, %WinTitle%
	if (ErrorLevel != "FAIL") {
		lResult:=ErrorLevel, VarSetCapacity(GUID,16,0), pdoc:=""
		if DllCall("ole32\CLSIDFromString", "wstr","{332C4425-26CB-11D0-B483-00C04FD90119}", "ptr",&GUID) >= 0 {
			DllCall("oleacc\ObjectFromLresult", "ptr",lResult, "ptr",&GUID, "ptr",0, "ptr*",pdoc)
			return ComObj(9,ComObjQuery(pdoc,IID,IID),1), ObjRelease(pdoc)
		}
	}
}

; https://www.autohotkey.com/boards/viewtopic.php?p=62120#p62120
FI_GetResource(ResName,ResType="10") {
   ; RT_RCDATA := 10 ;Fileinstall is always 10
   If !(A_IsCompiled)
      Return False
   If (HRSRC := DllCall("Kernel32.dll\FindResource", "Ptr", 0, "Str", ResName, "Ptr", ResType, "UPtr"))
   && (HRES := DllCall("Kernel32.dll\LoadResource", "Ptr", 0, "Ptr", HRSRC, "UPtr"))
      Return {Ptr: DllCall("Kernel32.dll\LockResource", "Ptr", HRES, "UPtr")
            , Size: DllCall("Kernel32.dll\SizeofResource", "Ptr", 0, "Ptr", HRSRC, "UInt")}
   Return False
}
