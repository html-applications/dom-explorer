---
### ![goup.png](http://html-applications.bitbucket.io/images/goup.png) [Return to HTML Application page](http://html-applications.bitbucket.io) ###
---
## ![hta.png](http://html-applications.bitbucket.io/images/hta.png)  [DOM Explorer](http://html-applications.bitbucket.io/dom-explorer/readme.html) ##
DOM Explorer for launched HTA and Internet Explorer local pages.

Исследователь DOM структуры запущенных hta приложений и локальных страниц, открытых в Internet Explorer.